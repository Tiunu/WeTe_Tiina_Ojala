var notes = (function() {
	var list = [];
	return {
		add: function(note) {
			if (!note) {
				return false;
			}
			else {
				var newNote = note.trim();
				if (newNote.length === 0) {
					return false;
				}
				else {
					list.splice(list.length, 0, {
						note: newNote,
						date: Date.now()
					});
					return true;
				}
			}
		},
		remove: function(index) {
			if (index === undefined) {
				return false;
			} else if (index >= list.length) {
				return false;
			}
			else {
				list.splice(index, 1);
				return true;
			}
		},
		count: function() {
			return list.length;
		},
		list: function() {
			return list;
		},
		find: function(str) {
			var foundNotes = [];
			for (var i = 0; i < list.length; i++) {
				if (list[i].note.includes(str)) {
					foundNotes.splice(list.length, 0, list[i]);
				}
			}
			if (foundNotes.length == 0) {
				return false;
			}
			else {
				return foundNotes;
			}
		},
		clear: function() {
			list.splice(0, list.length);
			return list.length;
		}
	}
}());



/*
var notes = {};

function add(note) {
	var newNote = note.trim();
	if (newNote.length === 0) {
		return false;
	}
	else {
		notes.splice(notes.length, 0, {
			note: newNote,
			date: Date()
		});
		return true;
	}
}

function remove(index) {
	if (index === undefined || index > notes.length) {
		return false;
	}
	else {
		notes.splice(index, 1);
		return true;
	}
}

function count() {
	return notes.length;
}

function list() {
	return notes;
}

function find(string) {
	var foundNotes = {};
	for (var i = 0; i < notes.length; i++) {
		if (notes[i].note === string) {
			foundNotes.splice(notes.length, 0, notes[i]);
		}
	}
	if (foundNotes.length == 0) {
		return false;
	}
	else {
		return foundNotes;
	}
}

function clear() {
	notes.splice(0, notes.length);
}
*/
