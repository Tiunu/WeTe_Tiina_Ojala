/*global notes*/
/* global expect*/

describe("notes module", function() {
	beforeEach(function() {
		notes.clear();
		notes.add('first note');
		notes.add('second note');
		notes.add('third note');
		notes.add('fourth note');
		notes.add('fifth note');
	});
// ADD 
	describe('adding a note', function() {
		it('should be able to add a new note', function() {
			expect(notes.add('sixth note')).toBe(true);
			expect(notes.count()).toBe(6);
		});
		it('should ignore blank notes', function() {
			expect(notes.add('')).toBe(false);
			expect(notes.count()).toBe(5);
		});
		it('should ignore notes containing only whitespace', function() {
			expect(notes.add('   ')).toBe(false);
			expect(notes.count()).toBe(5);
		});
		it('should require a string parameter', function() {
			expect(notes.add()).toBe(false);
			expect(notes.count()).toBe(5);
		});
	});
// REMOVE
	describe('deleting a note', function() {
		it('should be able to delete the first index', function() {
			expect(notes.remove(0));
			expect(notes.count()).toBe(4);
		});
		it('should be able to delete the last index', function() {
			expect(notes.remove(notes.length - 1));
			expect(notes.count()).toBe(4);
		});
		it('should return false if index is out of range', function() {
			expect(notes.remove(notes.length)).toBe(false);
		});
		it('should return false if the parameter is missing', function() {
			expect(notes.remove()).toBe(false);
		});
	});
// COUNT
	describe('counting notes', function() {
		it('should have 5 notes', function() {
			expect(notes.count()).toBe(5);
		});
		it('should have 0 notes', function() {
			expect(notes.clear());
			expect(notes.count()).toBe(0);
		});
		it('should have 2 notes', function() {
			expect(notes.clear());
			expect(notes.add('Note1'));
			expect(notes.add('Note1'));
			expect(notes.count()).toBe(2);
		});
	});
	// LIST
	describe('receiving notes', function() {
		it('should receive all notes (length 5)', function() {
			expect(notes.list().length).toBe(5);
		});
		it('should receive 3rd note', function() {
			expect(notes.list()[2].note).toBe('third note');
		});
	});
	// FIND
	describe('finding notes', function() {
		it('should receive notes with string "second note" (2nd)', function() {
			expect(notes.find('second note')[0].note).toBe('second note');
		});
		it('should receive notes with string "note" (all)', function() {
			expect(notes.find('note').length).toBe(5);
		});
		it('should receive notes with string "fi" (1st, 5th)', function() {
			expect(notes.find('fi').length).toBe(2);
		});
	});
	// CLEAR
	describe('clearing notes', function() {
	    it('clearing all notes', function() {
	    	expect(notes.clear()).toBe(0);
	    });
	});
});
