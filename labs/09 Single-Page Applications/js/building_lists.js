var div = document.createElement('div');


var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

//bullet + year
/*var list = document.createElement('ul');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('li');
	item.innerHTML = books[i].title + ", " + books[i].year;
	list.appendChild(item);*/

var table = document.createElement('table');
table.setAttribute("id", "taulu");
var tbody = document.createElement('tbody');

table.appendChild(tbody);

var thead = document.createElement('thead');
table.appendChild(thead);

var thbook = document.createElement('th');
thead.appendChild(thbook);
thbook.innerHTML = "BOOKS";

var thyear = document.createElement('th');
thead.appendChild(thyear);
thyear.innerHTML = "YEAR";

for (var i = 0; i < books.length; i++) {
	var row = document.createElement('tr');
	tbody.appendChild(row);
	
	var tdbookname = document.createElement('td');
	tdbookname.innerHTML = books[i].title;
	row.appendChild(tdbookname);
	
	var tdbookyear = document.createElement('td');
	tdbookyear.innerHTML = books[i].year;
	row.appendChild(tdbookyear);
}

window.onload = function addRowHandlers() {
    var table = document.getElementById("taulu");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler = 
            function(row) 
            {
                return function() { 
                                        var cell = row.getElementsByTagName("td")[0];
                                        var id = cell.innerHTML;
										div.innerHTML = id;
                                 };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
};

document.body.appendChild(div);
document.body.appendChild(table);
