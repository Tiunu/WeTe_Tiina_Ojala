<?php

class Dice {
    private  $faces;
    private  $freqs = array();
    private  $p;
    
    // Constructor
    public function __construct($faces, $p) {
        $this->faces = $faces;
        $this->p = $p;

    }
    
    public function cast() {
        if($this->p && $p<1.0){
            $chance = rand(0, 99)/100;
            echo $chance . ", ";
            if($chance <= $this->p){
                echo "MAKSIMITULOS ";
                $res = $this->faces;
                $this->freqs[$res]++;
                return $res;
            }
            else{
                echo "ALLE SILMALUVUN ";
                $res = rand(1, ($this->faces-1));
                $this->freqs[$res]++;
                return $res;
            }
            
        }
        else {
            $res = rand(1,$this->faces);
            $this->freqs[$res]++;
            return $res;
        }
    }
    
    public function getFreq($eyes) {
        $freq = $this->freqs[$eyes];
        if ($freq=="")
            $freq = 0;
        return $freq;
    }
    
    public function average($results, $throws){
        $avg = $results / $throws;
        return $avg;
    }

}

class PhysicalDice extends Dice {
    private $material;
    
    public function __construct($faces, $bias, $material) {
        parent::__construct($faces,$bias);
        $this->material = $material;
        echo $material;
    }
}


?>